# Homework submission template repository

This repository defines the preferred way to submit a homework
assignment in the course.  The reposity contains templates for

* theoretical questions,
* analysis in Stan, using PyStan,
* analysis in Infergo.

## How to use the repository

1. Create a bitbucket account if you do not have one.
2. Fork the repository.
3. Add your homework assignment partner and me (dtolpin on
   bitbucket) as collaborators.
4. Modify files in the repository to contain your solution.
5. Commit and tag your submission with tag hwX where X is the
   homework number. That is, homework assignment 1 will have tag
   hw1 (`git tag hw1`). 
6. Push your submission to bitbucket and notify me in a personal
   message on Slack.

## Files in the repository

### README.md

Contains this text, also posted on Slack. 

### solution.md

Write your answers to theoretical questions here. Refer to
instructions inside the template.

### solution.ipynb

A boilerplate template for Stan-based analysis. Implement and
test your PyStan analysis in this Jupyter notebook. You may
take a look at `examples/gmm/gmm.stan` in the course repository
as an example.

### Go files

* main.go
* model/model.go
* go.mod
* Makefile

A boilerplate template for Infergo-based analysis. Use
examples in http://bitbucket.org/dtolpin/infergo,
http://bitbucket.org/dtolpin/infergo-studies. 
