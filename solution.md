# Homework assignment X

## Question 1

### Problem 

Exercise 1. Problem description:

Conditional probability: suppose that if θ = 1, then y has a normal distribution with
mean 1 and standard deviation σ, and if θ = 2, then y has a normal distribution with
mean 2 and standard deviation σ. Also, suppose Pr(θ = 1) = 0.5 and Pr(θ = 2) = 0.5.

(a) For σ = 2, write the formula for the marginal probability density for y and sketch it.

(b) What is Pr(θ = 1|y = 1), again supposing σ = 2?

(c) Describe how the posterior density of θ changes in shape 
as σ is increased and as it is
decreased.

### Solution

We know that $$Pr(\theta=1)=Pr(\theta=2)=0.5$$. Also, we know that the value of $$\theta$$ is the $$\mu$$ of a normal distribution with a fixed $$\sigma$$.

a) Assuming $$\sigma$$=2, we can derive the marginal probability density function from the sum of all joint probability distributions over all values of $$\theta$$:
$$ p(y) = \sum p(y,\theta) = Pr(\theta=1)p(y|\theta=1) + Pr(\theta=2)p(y|\theta=2) = 0.5N(y|1,2^2) + 0.5N(y|2,2^2)$$ 

In order to sketch the marginal probability density function, we used python scipy library. The code and sketch can be found under the file "sketches.ipynb". 

b) Assuming $$\sigma=2$$ and knowing we're working with normal distributions, we'll use the normal distribution probability density function in order to compute $$Pr(\theta=1|y=1)$$.
We know that conditional probability can be computed using the joint probability divided by the marginal probability:

$$Pr(\theta=1|y=1)$$ = $$\frac{p(\theta=1 \cap y=1)}{p(y=1)}$$ = $$\frac{p(y=1 \cap \theta=1)}{p(y=1)}$$ = $$\frac{Pr(\theta=1)p(y=1|\theta=1)}{Pr(\theta=1)p(y=1|\theta=1) + Pr(\theta=2)p(y=1|\theta=2)}$$ = $$\frac{0.5N(1|1,2^2)}{0.5N(1|1,2^2) + 0.5N(1|2,2^2)}$$ = ~0.5312
* Note: We left the obvious part of using the reflexivity propery in the third step just to make things clear. Also, the exact calculation performed using python scipy's library and can be found under "hw1-extras.ipynb"

c) The shape of the posterior density of $$\theta$$ will be "wider" (not sure if that's the right term to use) as $$\sigma \to \infty $$, and will be "narrower" as $$\sigma \to 0$$. Obviously, because $$\sigma>0$$ we didn't take into consideration other cases.
We can learn from that behavior, that for example if we get the value of y=1 (as in 1b), then the $$Pr(\theta=1|y=1)$$ will be closer to 1 as $$\sigma \to \infty$$. Similarly, $$Pr(\theta=1|y=1)$$ will be closer to 0 as $$\sigma \to 0$$. 


## Question 2

### Problem

Exercise 4. Problem description:

Probability assignment: we will use the football dataset to estimate some conditional
probabilities about professional football games. There were twelve games with point
spreads of 8 points; the outcomes in those games were: −7, −5, −3, −3, 1, 6, 7, 13, 15,
16, 20, and 21, with positive values indicating wins by the favorite and negative values
indicating wins by the underdog. Consider the following conditional probabilities:
Pr(favorite wins| point spread = 8),
Pr(favorite wins by at least 8 | point spread = 8),
Pr(favorite wins by at least 8 | point spread = 8 and favorite wins).

(a) Estimate each of these using the relative frequencies of games with a point spread of
8.

(b) Estimate each using the normal approximation for the distribution of (outcome −
point spread).

### Solution
a) In order to estimate each of the probabilities using relative frequencies, we'll use the formula: $$Pr(A|B)=\frac{\#A and B}{\#A}$$

Therefore: 

$$Pr(Favorite wins|Point spread=8)=\frac{8}{12} = 0.67$$

$$Pr(Favorite wins by 8|Point spread=8)=\frac{5}{12} = 0.416$$

$$Pr(Favorite wins by 8|Point spread=8 and Favorite wins)=\frac{5}{8} = 0.625
$$

b) Assuming that this question regarding all the data of football point spreads (from 672 games), we know from section 1.6 that the data becomes from a normal distribution with $$\mu=0$$ and $$\sigma=14$$.
Therefore, in order to compute the probabilities asked, we'll use the normal distribution cumulative distribution function formula. 

$$Pr(Favorite wins|Point spread=8) = 1 - \phi(-8.5) = 0.728 $$

$$Pr(Favorite wins by 8|Point spread=8)= 1 - \phi(-0.5) = 0.514 $$

$$Pr(Favorite wins by 8|Point spread=8 \cap Favorite wins)=\frac{p(Favorite wins by 8 \cap Point spread =8 \cap Favorite wins)}{p(Point spread=8 \cap Favorite wins)}$$ = $$\frac{p(Favorite wins by 8 \cap Point spread = 8)}{p(Favorite wins \cap Point spread=8)}$$ = $$\frac{p(Favorite wins by 8|point spread=8)p(Point spread=8)}{p(Favorite wins|Point spread=8)p(Point spread=8)}$$=$$\frac{0.514}{0.728}$$ = 0.706

The calculations done using python's scipy library, and can be found under hw1-extras.ipynb.



## Question 3

### Problem
Excercise 6. Problem description:

Conditional probability: approximately 1/125 of all births are fraternal twins and 1/300
of births are identical twins. Elvis Presley had a twin brother (who died at birth). What
is the probability that Elvis was an identical twin? (You may approximate the probability
of a boy or girl birth as 0.5)

### Solution
First, we know that in order to be an identical twin you must be in the same gender.
So, the other twin should be a boy!

We know that: $$Pr(boy)= \frac{1}{2}, Pr(fraternal)=\frac{1}{125}, Pr(identical)=\frac{1}{300}$$

We calculate the probabilites depending on that the twin was a boy: 

$$Pr(boy \cap\ identical) = \frac{1}{2} * \frac{1}{300} = \frac{1}{600}$$

$$Pr(boy \cap\ fraternal) = \frac{1}{4} * \frac{1}{125} = \frac{1}{500}$$

 *Note*: We multiply by $\frac{1}{4}$ because there are 4 cases of fraternal twins: boy boy, boy girl, girl boy, girl girl and we're interested in the case of boy boy.
 
 We're interested in $Pr(identical | boy)$, which is:
 $$Pr(identical | boy) = \frac{Pr(boy \cap\ identical)}{Pr(boy \cap\ identical) + Pr(boy \cap\ fraternal)} = \frac{\frac{1}{600}}{\frac{1}{600} + \frac{1}{500}} = \frac{5}{11}$$
