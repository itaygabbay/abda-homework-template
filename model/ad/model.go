package model

import (
	"math"

	. "bitbucket.org/dtolpin/infergo/dist/ad"
	"bitbucket.org/dtolpin/infergo/ad"
)

type Entry struct {
	Company	string
	Count	int
	Average	float64
}

type Model struct {
	Data []Entry
}

type OriginalModel Model

func (m *OriginalModel) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var logp float64
	ad.Assignment(&logp, ad.Value(0.))
	var logMu float64
	ad.Assignment(&logMu, &x[0])
	var logTau float64
	ad.Assignment(&logTau, &x[1])
	var tau float64
	ad.Assignment(&tau, ad.Elemental(math.Exp, &logTau))
	var logEta []float64

	logEta = x[2:]
	var eta []float64

	eta = make([]float64, len(m.Data))
	for i := range eta {
		ad.Assignment(&eta[i], ad.Elemental(math.Exp, &logEta[i]))
	}
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Call(func(_ []float64) {
		Cauchy.Logp(0, 0, 0)
	}, 3, ad.Value(0), ad.Value(5), &logTau)))
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Call(func(_ []float64) {
		Normal.Logps(0, 0, logEta...)
	}, 2, &logMu, &tau)))

	for i := range m.Data {
		var alpha float64
		ad.Assignment(&alpha, ad.Value(float64(m.Data[i].Count)))
		var beta float64
		ad.Assignment(&beta, ad.Arithmetic(ad.OpDiv, &alpha, &eta[i]))
		ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Call(func(_ []float64) {
			Gamma.Logp(0, 0, 0)
		}, 3, &alpha, &beta, &m.Data[i].Average)))
	}

	return ad.Return(&logp)
}

type ModifiedModel Model

func (m *ModifiedModel) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var logp float64
	ad.Assignment(&logp, ad.Value(0.))
	var logMu float64
	ad.Assignment(&logMu, &x[0])
	var logTau float64
	ad.Assignment(&logTau, &x[1])
	var tau float64
	ad.Assignment(&tau, ad.Elemental(math.Exp, &logTau))
	var logEta []float64

	logEta = x[2:]
	var eta []float64

	eta = make([]float64, len(m.Data))
	for i := range eta {
		ad.Assignment(&eta[i], ad.Elemental(math.Exp, &logEta[i]))
	}
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Call(func(_ []float64) {
		Normal.Logp(0, 0, 0)
	}, 3, ad.Value(0), ad.Value(2), &logTau)))
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Call(func(_ []float64) {
		Normal.Logps(0, 0, logEta...)
	}, 2, &logMu, &tau)))

	for i := range m.Data {
		var alpha float64
		ad.Assignment(&alpha, ad.Value(float64(m.Data[i].Count)))
		var beta float64
		ad.Assignment(&beta, ad.Arithmetic(ad.OpDiv, &alpha, &eta[i]))
		ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Call(func(_ []float64) {
			Gamma.Logp(0, 0, 0)
		}, 3, &alpha, &beta, &m.Data[i].Average)))
	}

	return ad.Return(&logp)
}
