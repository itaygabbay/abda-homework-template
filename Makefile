all: solution

solution: model/ad/model.go main.go
	go build -o $@ .
	./solution

model/ad/model.go: model/model.go
	deriv model

clean:
	rm -f ./hello model/ad/*.go
