module bitbucket.org/itaygabbay/abda-homework-template

go 1.14

require (
	bitbucket.org/dtolpin/infergo v0.8.4
	gonum.org/v1/plot v0.7.0
)
