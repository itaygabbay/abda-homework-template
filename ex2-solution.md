# Homework assignment 2

### Question 1 - Exercise 2.2 from Chapter 2 ###
Predictive distributions: consider two coins, C1 and C2, with the following characteristics:

$$ \Pr(heads|C1) = 0.6$, and $\Pr(heads|C2) = 0.4 $$.

Choose one of the coins at random and
imagine spinning it repeatedly. Given that the first two spins from the chosen coin are tails, what is the expectation of the number of additional spins until a head shows up?

### Answer ###

Let denote the variable C as the index of the chosen coin (i.e., C=1 says choosing C1). Assuming uniform distribution regarding coin selection, we can say that 
$$\Pr(C=1)=\Pr(C=2)=1/2$$.
In order to compute the expectation of number of additional spins until a head shows up, we'll consider N to be a random variable from a Geometric distribution, where we model N as the number of Bernoulli trials to gen on success (where we denote tail as a failure and head as a success). Also, we know that the mean of a Geometric distribution is: $$\frac{1}{p}$$ where p is the success probability.
Therefore, we can calculate the expectation of number of spins until a head for each coin:

$$ E[N|TT, C=1] = \frac{1}{0.6} $$
$$ E[N|TT, C=2] = \frac{1}{0.4} $$

Also, we need to calculate what is the probability for the coin to be the first or the second coin, given that we saw 2 tails:

$$ \Pr(C=1|TT) = \frac{\Pr(TT|C=1)\Pr(C=1)}{\Pr(TT|C=1)\Pr(C=1)+\Pr(TT|C=2)\Pr(C=2)} = \frac{0.4^2*0.5}{0.4^2*0.5 + 0.6^2*0.5} = \frac{4}{13}$$ 

$$\Pr(C=2|TT) = \frac{\Pr(TT|C=2)\Pr(C=2)}{\Pr(TT|C=1)\Pr(C=1)+\Pr(TT|C=2)\Pr(C=2)} = \frac{0.6^2*0.5}{0.4^2*0.5 + 0.6^2*0.5} = \frac{9}{13} = 1-\Pr(C=1|TT)$$

Now, we'll calculate the expected value of expected number of additional spins for each coin:

$$E[E[N|TT,C]|TT] = \Pr(C=1|TT)E[N|TT,C=1) + \Pr(C=2|TT)E[N|TT,C=2] = \frac{4}{13}\frac{1}{0.6} + \frac{9}{13}\frac{1}{0.4} = 2.243$$ 


### Question 2 - Exercise 2.12 from Chapter 2 ###
Discrete data: Table 2.2 gives the number of fatal accidents and deaths on scheduled
airline flights per year over a ten-year period. We use these data as a numerical example
for fitting discrete data models.

(a) Assume that the numbers of fatal accidents in each year are independent with a
Poisson($$\theta$$) distribution. Set a prior distribution for $$\theta$$ and determine the posterior distribution based on the data from 1976 through 1985. Under this model, give a 95\% predictive interval for the number of fatal accidents in 1986. You can use the normal approximation to the gamma and Poisson or compute using simulation.

(b) Assume that the numbers of fatal accidents in each year follow independent Poisson distributions with a constant rate and an exposure in each year proportional to the number of passenger miles flown. Set a prior distribution for $$\theta$$ and determine the posterior distribution based on the data for 1976–1985. (Estimate the number of passenger miles flown in each year by dividing the appropriate columns of Table 2.2. and ignoring round-off errors.) Give a 95\% predictive interval for the number of fatal accidents in 1986 under the assumption that $$8 *10^{11}$$ passenger miles are flown that year.

(c) Repeat (a) above, replacing ‘fatal accidents’ with ‘passenger deaths.’

(d) Repeat (b) above, replacing ‘fatal accidents’ with ‘passenger deaths.’

(e) In which of the cases (a)–(d) above does the Poisson model seem more or less reasonable? Why? Discuss based on general principles, without specific reference to the
numbers in Table 2.2.
Incidentally, in 1986, there were 22 fatal accidents, 546 passenger deaths, and a death
rate of 0.06 per 100 million miles flown. We return to this example in Exercises 3.12,
6.2, 6.3, and 8.14.

### Answer ###

a. Since we know that our data, y, sampled from a Poisson distribution, we'll select a conjugate Gamma uninformative prior with $$\alpha=\beta=1$$. Therefore, our posterior will be of the form: $$Gamma(\alpha+\Sigma y , \beta+n) = Gamma(239, 11)$$, Because 238 is the sum of all y values over n=10 years.
In order to use the normal approximation for calculating the confidence interval, we'll calculate the mean and variance of the posterior:

$$E(\theta|y)=\frac{239}{11} = 21.72 , Var(\theta|y)=\frac{239}{11^2} =  1.97$$

Now, we'll calculate the mean and variance of the posterior predictive distribution of P (denoting P as the predicted value):

$$E(P|y) = E(E[P|\theta,y]|y) = E[E[P|\theta]|y = E[\theta|y] = 21.72$$

The first equality derives from the law of total expectation, the second equality derives from the fact that P is independent of y given $$\theta$$ and the third equality from the fact that our posterior predictive distribution is a Poisson distribution (therefore the mean is $$\theta$$).

Also, we'll calculate the variance:

$$Var(P|y)=E[Var(P|\theta,y)|y]+ Var(E[P|\theta,y]|y) = E[Var(P|\theta)|y] + Var(E[P|\theta]|y) = E[\theta|y]+Var(\theta|y) =21.72 + 1.97 = 23.7 = 4.86^2$$ 

Those equalities follow the same logic we used for calculated $$E(P|y)$$ yet with the law of total variance, and the mean and variance formulas of Poisson distribution.

Thus, normal approximation gives 95% confidence interval for P of [12.9,31.3]. Yet, P is an integer, therefore it should be: [12, 32].

b. In order to calculate the amount of passenger miles flown at reach year, We divide the Passenger Deaths by the Death Rate at each year and we receive the estimated number of deaths per 100 million passenger miles. Then, we multiply that number by 100 million and get the (estimated) passenger miles flown at each year. 

Therefore, the data sampled from a Poisson with parameters n and $\theta$ where n is the amount of passenger miles flown and $\theta$ is the exposure. We again use Gamma(1,1) as our prior distribution and the posterior is: $$Gamma(239, $5.716 * 10^{12})$$, where: $$\Sigma n = 5.716*10^{12}$$.

Also, same as we done in a., the posterior predictive distribution is a Poission distribution, and for 1986 we know that it's from the form: Poisson($$8*10^{11}\theta$$). In order to compute it's 95% confidence interval, we'll find it's mean and variance and use normal approximation. Again, we'll denote P as the predicted value for 1986. 
Calculating the posterior mean and variance:

$$E(\theta|y)=\frac{239}{5.716*10^{12}} = 4.164*10^{-11}, Var(\theta|y)=\frac{239}{{5.716*10^{12}}^{2}} = 0.731*10^{-24} $$

Therefore, the mean and variance of the posterior predictive distribution:

$$E(P|y) = E(E(P|\theta)|y) = E(8*10^{11}\theta|y) = 8*10^{11}*4.164*10^{-11} = 33.3 $$

$$Var(P|y) = E(Var(P|\theta)|y) + Var(E(P|\theta)|y) = E(8*10^{11}\theta|y) + Var(8*10^{11}|y) = 8*10^{11}*4.164*10^{-11} + {8*10^{11}}^{2}*0.731*10^{-24} = 37.04 = 6.08^{2}$$

Thus, normal approximation gives 95% confidence interval for P of [21.38,45.21]. Yet, P is an integer, therefore it should be: [21, 46].

c. Same as a, yet this time the posterior is Gamma(6920,11) (where $$6920=\Sigma n$$ where n denotes passenger deaths).

Posterior mean and variance:

$$E(\theta|y)=\frac{6920}{11} = 629.1 , Var(\theta|y)=\frac{6920}{11^2} =  57.2 $$

Predictive posterior mean and variance:

$$E(P|y) = E[\theta|y] = 629.1$$

$$Var(P|y)= E[\theta|y]+Var(\theta|y) = 629.1 + 57.2 = 686.3 = 26.19^2$$

Thus, normal approximation gives 95% confidence interval for P of [577.76,680.4]. Yet, P is an integer, therefore it should be: [577, 681].

d. Same as b, yet this time the posterior is $$Gamma(6920,5.716*10^{12})$$. 

Posterior mean and variance:

$$E(\theta|y)=\frac{6920}{5.716*10^{12}} = 1.21*10^{-9}$$, $$Var(\theta|y)=\frac{6920}{5.716*10^{12}^{2}} = 0.211*10^{-22} $$

Predictive posterior mean and variance:

$$E(P|y) = E(8*10^{11}\theta|y) = 8*10^{11}*1.21*10^{-9} = 968 $$

$$Var(P|y)= E(8*10^{11}\theta|y) + Var(8*10^{11}|y) = 968 + {8*10^{11}}^{2}*0.211*10^{-22} = 31.38^{2}$$

Thus, normal approximation gives 95% confidence interval for P of [906.48,1029.51]. Yet, P is an integer, therefore it should be: [906, 1030].

e. Assuming that each year the number of passenger miles flown increases, it seems more reasonable to take that into account, thus I think that b (or d) seems more reasonable because it models the Poisson distribution with rate and exposure, where the exposure is in terms of passenger miles flown. 


### Question 3 - Exercise 3.3 from Chapter 3 ###
Estimation from two independent experiments: an experiment was performed on the effects of magnetic fields on the flow of calcium out of chicken brains. Two groups of chickens were involved: a control group of 32 chickens and an exposed group of 36 chickens. One measurement was taken on each chicken, and the purpose of the experiment was to measure the average flow µc in untreated (control) chickens and the average flow
µt in treated chickens. The 32 measurements on the control group had a sample mean of 1.013 and a sample standard deviation of 0.24. The 36 measurements on the treatment group had a sample mean of 1.173 and a sample standard deviation of 0.20.

(a) Assuming the control measurements were taken at random from a normal distribution
with mean µc and variance $$\sigma^{2}$$c, what is the posterior distribution of µc? Similarly, use the treatment group measurements to determine the marginal posterior distribution of µt. Assume a uniform prior distribution on (µc, µt, log σc, log σt).

(b) What is the posterior distribution for the difference, µt − µc? To get this, you may sample from the independent t distributions you obtained in part (a) above. Plot a histogram of your samples and give an approximate 95% posterior interval for µt −µc.
The problem of estimating two normal means with unknown ratio of variances is called the Behrens–Fisher problem.

### Answer ###

We know that the data was taked from a normal distribution, therefore, the likelihood is of the form: 

$$p(y|\mu_c,\sigma_c,\mu_t,\sigma_t) =   \prod_{i=1}^{32}N(y_{ci}|\mu_c,\sigma_c)\prod_{i=1}^{36}N(y_{ti}|\mu_t,\sigma_t) $$

Also, we know that we have a Uniform prior on all 4 parameters, therefore, the posterior is:

$$ p(\mu_c,log\sigma_c,\mu_t,log\sigma_t|y) = p(\mu_c,log\sigma_c,\mu_t,log\sigma_t)p(y|\mu_c,\sigma_c,\mu_t,\sigma_t) = \prod_{i=1}^{32}N(y_{ci}|\mu_c,\sigma_c)\prod_{i=1}^{36}N(y_{ti}|\mu_t,\sigma_t) $$

And since the two distribution factors of the posterior are independent, the first one is the posterior of $$p(\mu_c,log\sigma_c|y)$$ and the second is of $$p(\mu_t,log\sigma_t|y)$$ . Therefore, we can describe their marginal posterior distribution to be the $$t_n-1$$ distribution, as follows:

$$p(\mu_c|y) = t_{31}(1.013,0.24^{2}/32)$$
$$p(\mu_t|y) = t_{35}(1.174,0.20^{2}/36)$$

b.
The graphical representation and the 95% confidence interval is computed using sampling from the posteriors defined in a, and can be found uner 'ex2-extras.ipynb'.